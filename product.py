# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @classmethod
    def set_products_lot_required(cls):
        pool = Pool()
        RequiredLot = pool.get('product.template-stock.lot.type')
        to_create = []

        query = """
        select id from product_template where id not in (
            select template from public."product_template-stock_lot_type" 
            group by template 
            having count(template)=5
        )"""

        cursor = Transaction().connection.cursor()
        cursor.execute(query)
        fetch = cursor.fetchall()
        product_ids = [x[0] for x in fetch]
        for product in product_ids:
            for num in range(1, 6):
                to_create.append({'template': product, 'type': num})
        RequiredLot.create(to_create)
