# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from decimal import Decimal
from .log import Logging
from functools import wraps
from trytond.tools import grouped_slice
from trytond.modules.product import price_digits
import sys

_ZERO = Decimal('0.0')
DEFAULT_SLICE_SIZE = 100000

def _round(value):
    if not isinstance(value, Decimal):
        value = Decimal(str(value))
    return value.quantize(Decimal(str(10 ** - price_digits[1])))


class Move(metaclass=PoolMeta):
    __name__ = "stock.move"
    # __name__ = 'electrans.tasks'

    @classmethod
    def _set_initial_moves_to_from_Industry(cls):
        '''
        Change from_location from lost_and_found to supplier and set unit_price=cost_price
        for all the first inventory moves done
        '''
        print(sys._getframe().f_code.co_name + " start...")
        with Transaction().connection.cursor() as cursor:

            # Create industry location id if it doens't exist yet
            Location = Pool().get('stock.location')
            industry_locations = Location.search([('name', '=', 'Industry')])
            if industry_locations:
                industry_location, = industry_locations
            else:
                industry_location, = Location.create([{'name': 'Industry', 'code': 'IND', 'type': 'supplier'}])

            # Update stock_move if not done yet; previosly make a backup
            query = "select * from pg_tables where tablename='stock_move__backup'"
            cursor.execute(query)
            if cursor.rowcount == 0:
                # Backup
                query = "select * into stock_move__backup from stock_move;"
                cursor.execute(query)
                # Update from_location and unit_price
                query = "update stock_move" + \
                    " set from_location = " + str(industry_location.id) + \
                    " where from_location = 7 and effective_date < '2016-7-13' and origin like 'stock.inventory.line,%'"
                cursor.execute(query)
            else:
                print("...stock_move__backup already exists! Skipping...")
        print(sys._getframe().f_code.co_name + " end...")

    @classmethod
    def _reset_product_cost_prices(cls):
        '''
        Truncate the product_cost_price table
        '''
        print(sys._getframe().f_code.co_name + " start...")
        with Transaction().connection.cursor() as cursor:

            # Backup products_cost_price table (only if doesn't exists yet)
            query = "select * from pg_tables where tablename='product_cost_price__backup'"
            cursor.execute(query)
            if cursor.rowcount == 0:

                # Backup
                query = "select * into product_cost_price__backup from product_cost_price"
                cursor.execute(query)

                # Truncate table
                query = "truncate table product_cost_price restart identity;"
                cursor.execute(query)
            else:
                print("...stock_move__backup already exists! Skipping...")
        print(sys._getframe().f_code.co_name + " end...")

    @classmethod
    def _update_products_cost_price_method(cls):
        '''
        Change the price_cost_method=average for all the products
        '''
        print(sys._getframe().f_code.co_name + " start...")
        # Backup products_cost_price_method table (only if doesn't exists yet)
        with Transaction().connection.cursor() as cursor:
            query = "select * from pg_tables where tablename='product_cost_price_method__backup'"
            cursor.execute(query)
            if cursor.rowcount == 0:
                query = "select * into product_cost_price_method__backup from product_cost_price_method;"
                cursor.execute(query)
            else:
                print(sys._getframe().f_code.co_name + " end...")

        # Set all products cost_price_method to average
        pool = Pool()
        PriceMethod = pool.get('product.cost_price_method')
        products_w_cost_price_ne_avg = PriceMethod.search([('cost_price_method', '!=', 'average')])
        PriceMethod.write(products_w_cost_price_ne_avg, {'cost_price_method': 'average'})
        print(sys._getframe().f_code.co_name + " end...")

    @classmethod
    def _setup_recompute_costs(cls):
        cls._set_initial_moves_to_from_Industry()
        cls._reset_product_cost_prices()
        cls._update_products_cost_price_method()

    @classmethod
    def recompute_costs(cls, *args, **kwargs):
        '''
        Recompute all the product costs, updating moves and product cost prices.
        **kwargs:
            log_path: Path for logging and stock csv files (default '.')
            limit: Limit (default no limit)
            offset: Offset (default = 0)
            slice_size: Slice size (default DEFAULT_SLICE_SIZE)
            skip_non_produced_products: (default=False)
            product: Recompute only moves of the product (default=None)
            reset_product_cost_price__history (default=True)
            skip_internal_moves (default=True)

        This method can be invoked using trytond-console, i.e.:
        $ ./trytond/bin/trytond-console -c trytond.conf -d electrans
        >>> from trytond.pool import Pool
        >>> Move = Pool().get('stock.move')
        >>> Move.recompute_costs(skip_non_produced_products=True, limit=10)
        [working...]
        >>> transaction.commit()
        The last line will persist the changes.
        '''

        log_path = kwargs.get('log_file_path', '.')
        # assert log_path, "It is needed a path for log file as a first argument.\n" \n
        #    "and stock csv files."
        limit = kwargs.get('limit')
        offset = kwargs.get('offset', 0)
        slice_size = kwargs.get('slice_size', DEFAULT_SLICE_SIZE)
        skip_non_produced_products = kwargs.get('skip_non_produced_products', False)
        product_to_recompute = kwargs.get('product', None)
        reset_product_cost_price__history = kwargs.get('reset_product_cost_price__history', True)
        skip_internal_moves = kwargs.get('skip_internal_moves', True)

        def log(func):
            '''
            Decorator to trace the funtions calls
            '''
            @wraps(func)
            def wrapper(*args, **kwargs):
                text = " >" + func.__name__ + "()"
                logging.add(0, text)
                return func(*args, **kwargs)
            return wrapper

        def search_cost_price_in_Industry(product):
            '''
            In electrans34/modules/electrans/synchronization.py#398:

            [...]
            cost_price = row['PrecioCosteStandard']
            if row['TipoArticulo'] in (3, 4):
                cost_price = row['PrecioCompra']
            value['cost_price'] = field_round(cost_price, digits)
            [...]

            But Valero/Rosa say (2019/04/11) that it is better 'ultimopreciocoste' (instead of 'preciocompra')
            if 'tipoarticulo' in (3,4), and 'preciocostestandard' otherways (see view industry.marticulo_cost)
            '''
            query = "SELECT codigoarticulo, cost_price from industry.marticulo_cost"
            if product:
                query = query + " WHERE codigoarticulo = '" + str(product.code) + "'"
            cursor.execute(query)
            row = cursor.fetchone()
            if row:
                return Decimal(row[1])

        def search_unit_price_in_invoices(move):
            query = "select i.unit_price, i.unit " + \
                "from stock_move m " + \
                "join purchase_line p on m.origin='purchase.line,'||p.id " + \
                "join account_invoice_line i on i.origin='purchase.line,'||p.id " + \
                "where i.invoice is not null and m.id=" + str(move.id)
            cursor.execute(query)
            row = cursor.fetchone()
            if row:
                uom, = Uom.search([
                    ('id', '=', row[1]),
                ])
                return row[0], uom
            else:
                return None, None

        def search_unit_price_in_purchases(move):
            # Search in purchase lines previous to move. If found one that do not have
            # other moves return this, else return de last one found.
            query = "select pl.unit_price, pl.unit " + \
                "from purchase_line pl " + \
                "join purchase_purchase pu on pu.id=pl.purchase and pu.state in ('done', 'processing') " + \
                "left join stock_move m on m.origin='purchase.line,'||pl.id and m.state='done' " + \
                "where pl.product=" + str(move.product.id) + " and pl.create_date<'" + str(move.create_date) + "' " + \
                "order by (case when m.id is null then 1 else 0 end) desc, pl.id desc " + \
                "limit 1"
            cursor.execute(query)
            row = cursor.fetchone()
            if row:
                uom, = Uom.search([
                    ('id', '=', row[1]),
                ])
                return row[0], uom
            else:
                return None, None

        # @log The return doesn't work
        def check_unit_price(move):
            '''
            Returns unit_price.
            When from_location = 'supplier' => The move.unit_price is the good
            one (unless it was 0 => search in purchases, invoices and finally
            in Industry.).
            When from_location = 'production' => The move.unit_price has to be
            recomputed becase it depends on the inputs (that could be changed)
            When to_location = 'supplier' => ???
            Otherways, unit_price doesn't matter
            '''
            logging.add(0, " >check_unit_price()")

            # Move from Industry
            if move.from_location.id == industry_location.id:
                logging.add(0, ": move from Industry => Search unit price in Industry... ")
                unit_price = search_cost_price_in_Industry(move.product)
                if unit_price:
                    logging.add(0, "Found!")
                    return _round(unit_price)
                else:
                    # TODO: Search in other invoices?
                    logging.add(2, "Unit_price=0!" if unit_price == 0 else "NOT FOUND!")
                    return _ZERO  # Assure it is not None...

            # Move from supplier other than Industry
            elif move.from_location.type == 'supplier':  # and move.from_location.id <> industry_location.id
                # Move from suplier without origin, search in previous purchases and if not found then in Industry
                if not move.origin:
                    logging.add(1, ": move from supplier without origin => Search in previous purchases... ")
                    unit_price, uom = search_unit_price_in_purchases(move)
                    if unit_price:
                        logging.add(1, "Found!")
                        return _round(Uom.compute_price(uom, unit_price, move.uom))
                    else:
                        logging.add(1, "Not found, Search in Industry... ")
                        unit_price = search_cost_price_in_Industry(move.product)
                        if unit_price:
                            logging.add(1, "Found!")
                            return _round(unit_price)
                        else:
                            logging.add(1, "Not found, Search in product_cost_price... ")
                            product_cost_price = move.product.get_multivalue('cost_price', **move._cost_price_pattern)
                            if product_cost_price:
                                logging.add(1, "Found!")
                                return _round(Uom.compute_price(move.product.default_uom, product_cost_price, move.uom))
                            else:
                                logging.add(2, "NOT FOUND!")
                                return move.unit_price  # _ZERO # Assure it is not None...
                # If move origin is Non-conformity, the unit_price has to be reassined from product cost price
                elif move.origin.__class__.__name__ == 'nonconformity.line':
                    logging.add(1, "Non-conformity. Unit_price = cost price!")
                    product_cost_price = move.product.get_multivalue('cost_price', **move._cost_price_pattern)
                    return _round(Uom.compute_price(move.product.default_uom, product_cost_price, move.uom))
                # move origin is purchase_line
                else:
                    if move.unit_price:
                        # TODO: Compare with invoice?
                        logging.add(0, ": move from supplier, unit_price > 0 => DO NOTHING.")
                        return move.unit_price
                    else:
                        logging.add(1, ": move from supplir, unit_price == 0 (or None) => Search in Invoices... ")
                        unit_price, uom = search_unit_price_in_invoices(move)
                        if unit_price:
                            logging.add(1, "Found!")
                            return _round(Uom.compute_price(uom, unit_price, move.uom))
                        else:
                            logging.add(1, "Not found, Search in product_cost_price... ")
                            # TODO: Search in other invoices?
                            product_cost_price = move.product.get_multivalue('cost_price', **move._cost_price_pattern)
                            if product_cost_price:
                                logging.add(1, "Found!")
                                return _round(Uom.compute_price(move.product.default_uom, product_cost_price, move.uom))
                            else:
                                logging.add(2, "NOT FOUND!")
                                return _ZERO  # Assure it is not None...

            # Move to supplier
            elif move.to_location.type == 'supplier':
                if move.unit_price:
                    logging.add(0, ": move to supplier, unit_price > 0 => DO NOTHING.")
                    return move.unit_price
                else:
                    logging.add(1, ": move to supplier, unit_price == 0 (or None) => Search product.cost_price.")
                    product_cost_price = move.product.get_multivalue('cost_price', **move._cost_price_pattern)
                    if product_cost_price:
                        logging.add(1, ". Cost_price found in product_cost_price!")
                        return _round(Uom.compute_price(move.product.default_uom, product_cost_price, move.uom))
                    else:
                        logging.add(2, ". Cost_price NOT FOUND in product_cost_price!")
                        return _ZERO  # Assure it is not None...

            # Move from production
            elif move.from_location.type == 'production':
                if move.product == move.production_output.product:
                    logging.add(0, ": move from production (main product)")
                    query = "select quantity from stock_move " +\
                        " where production_output = " + str(move.production_output.id) +\
                        " and product = " + str(move.product.id)
                    cursor.execute(query)
                    quantities = [Decimal(o[0]) for o in cursor.fetchall()]
                    total_produced_quantity = sum(quantities)
                    if total_produced_quantity == 0:  # Unexpected case!
                        logging.add(2, ": total_produced_quantity == 0!")
                        return _round(move.production_output.cost)
                    else:
                        unit_price = move.production_output.cost / total_produced_quantity
                        if unit_price != 0:
                            logging.add(0, ", moves: " + str(len(quantities)) + " quantity: " + str(total_produced_quantity))
                            return _round(Uom.compute_price(move.production_output.uom, unit_price, move.uom))
                        else:
                            logging.add(1, ", production_cost == 0!")
                            return _ZERO
                else:
                    logging.add(0, ": move from production (subproduct) => unit_price must be UPDATED TO 0!")
                    return _ZERO

            # Other kind of move
            else:
                logging.add(0, ": move not from/to purchase neither from production => DO NOTHING (unit_price doesn't matter).")
                return move.unit_price

        @log
        def update_cost(move, update_unit_price=False, update_cost_price=False, update_product_cost_price=False, unit_price=None, cost_price=None):
            '''
            Updates move.cost_price move.unit_price and product.cost_price when required.
            '''

            if unit_price and move.unit_price != unit_price:
                logging.add(0, ", unit_price changed: " + str(move.unit_price) + " -> " + str(unit_price))
                update_unit_price = True
                move.unit_price = unit_price

            if cost_price and move.cost_price != cost_price:
                logging.add(0, ", cost_price changed: " + str(move.cost_price) + " -> " + str(cost_price))
                update_cost_price = True
                move.cost_price = cost_price

            # Update move cost_price, and unit_price if required
            if update_unit_price or update_cost_price:
                query = \
                    "update stock_move set " + \
                    (" cost_price = " + str(move.cost_price) if update_cost_price else "") + \
                    ("," if update_cost_price and update_unit_price else "") + \
                    (" unit_price = " + str(move.unit_price) if update_unit_price else "") + \
                    " where id = " + str(move.id)
                cursor.execute(query)
                logging.add(1, " => UPDATED!")

            else:
                logging.add(0, "Move up to date.")

            # Update product_cost_price if is required
            if update_product_cost_price:
                logging.add(1, "; update product cost price (" + str(move.product.id) + ") -> " + str(cost_price))
                move.product.set_multivalue('cost_price', cost_price, save=True, **move._cost_price_pattern)
            else:
                logging.add(0, "Product cost up to date.")

        @log
        def check_move(move):
            '''
            Checks move.unit_price, move.cost_price and product.cost_price.
            '''
            logging.add(0, "[" + str(move.from_location.type)[:3] + "->" + str(move.to_location.type)[:3] + "]")

            #if skip_non_produced_products and move.product not in produced_products:
            #    logging.add(1, "Move skipped!")
            #    return

            if move.to_location.type == 'storage' and move.from_location.type != 'storage':
                qty = move.quantity
            elif move.from_location.type == 'storage' and move.to_location.type != 'storage':
                qty = -move.quantity
            else:
                qty = 0
            product_qty = stock[move.product.id]['quantity'] if stock.get(move.product.id) else 0

            qty_in_default_uom = Uom.compute_qty(move.uom, qty, move.product.default_uom)

            # If cost_price_method is not average, don't recompute cost price
            if (move.product.cost_price_method != 'average'):
                logging.add(1, "product.cost_price_method=" + move.product.cost_price_method + "!")

            else:
                # Moves that affect the product cost_price
                if (move.from_location.type in ('supplier', 'production')
                        and move.to_location.type == 'storage') \
                    or (move.from_location.type == 'storage'
                        and move.to_location.type == 'supplier'):

                    logging.add(0, ". Move affect product cost price; calculate it... ")

                    context['stock_date_end'] = move.effective_date  # I think it is not necessary yet.
                    with Transaction().set_context(context):
                        unit_price = check_unit_price(move)  # Ojo con _Zero y None!!! Comprobar!!!
                        unit_price_in_default_uom = Uom.compute_price(move.uom, unit_price, move.product.default_uom)

                        product_cost_price = move.product.get_multivalue('cost_price', **move._cost_price_pattern)

                    # Copied from _compute_product_cost_price()
                    direction = 'in' if move.from_location.type in ('supplier', 'production') else 'out'
                    if product_qty + qty_in_default_uom > 0 and product_qty >= 0:
                        new_cost_price = (
                            (product_cost_price * Decimal(product_qty)) + (unit_price_in_default_uom * Decimal(qty_in_default_uom))
                            ) / Decimal(product_qty + qty_in_default_uom)
                    elif direction == 'in':
                        new_cost_price = unit_price
                    elif direction == 'out':
                        new_cost_price = product_cost_price
                    new_cost_price = _round(new_cost_price)

                    logging.add(0, "Calculated! ")

                    update_cost(move,
                        update_product_cost_price=(product_cost_price != new_cost_price),
                        unit_price=unit_price,
                        cost_price=new_cost_price)

                # Special case: Add stock from lost_and_found and there is no previous move =>
                # Search cost price from Industry.
                elif move.from_location.type == 'lost_and_found' and not stock.get(move.product.id):
                    logging.add(1, ". Move from lost_found and it is the first for product! => Search cost in Industry... ")
                    industry_cost_price = search_cost_price_in_Industry(move.product)
                    if industry_cost_price:
                        logging.add(1, "Found!")
                        industry_cost_price = _round(industry_cost_price)
                        update_cost(move,
                            update_unit_price=False,
                            update_product_cost_price=True,
                            cost_price=industry_cost_price)
                    else:
                        logging.add(2, ": Not Found!")
                        #update_cost(move, update_product_cost_price=False, cost_price=0)

                # Moves that don't affect the product cost_price
                else:
                    logging.add(0, ". Move doesn't affect product cost price. Check move cost... ")
                    cost_price = move.product.get_multivalue('cost_price', **move._cost_price_pattern)
                    update_cost(move, cost_price=cost_price)

            # Update stock dictionary
            if qty:
                new_quantity = product_qty + qty_in_default_uom
                #255.9951 - 0.0245 = 255.97060000000002, so round it!
                new_quantity = round(new_quantity, 4)
                stock[move.product.id] = {
                    'product': move.product.id,
                    'quantity': new_quantity,
                    'cost_price': move.cost_price,
                    'write_date': move.write_date }
                logging.add(0, " >Update stock var (product:" + str(move.product.id) + "): " + str(product_qty) + "->" + str(new_quantity))

        def load_offset_stock_from_csv(file_name):
            '''
            Load stock dictionary with previous data from a csv. The csv has to be a file with name
            stock_nnn.csv where nnn is the number of records skiped (offset).
            '''
            import csv
            stock = {}  
            with open(file_name, 'rt') as f:
                reader = csv.DictReader(f, fieldnames=['product', 'quantity', 'cost_price', 'write_date'],
                    quoting=csv.QUOTE_NONNUMERIC, lineterminator="\n",
                    delimiter=',', quotechar='"')
                next(reader)  # skip header
                for row in reader:
                    stock[row['product']] = row
            return stock

        def save_stock_to_csv(stock, file_name):
            '''
            Save stock dictionary to a csv, adding previous data if there is an offset
            '''
            import csv
            with open(file_name, 'at') as f:
                writer = csv.DictWriter(f, fieldnames=['product', 'quantity', 'cost_price', 'write_date'], 
                    extrasaction='ignore', quoting=csv.QUOTE_NONNUMERIC, lineterminator="\n", delimiter=',', quotechar='"')
                writer.writeheader()
                for keys, values in list(stock.items()):
                    writer.writerow(values)


        #############################
        # Main function starts here #
        #############################

        # Call setup
        # cls._setup_recompute_costs()

        # Initialize needed classes
        pool = Pool()
        Product = pool.get('product.product')
        Location = pool.get('stock.location')
        Uom = pool.get('product.uom')

        # Instance cursor to be used here and in other functions
        cursor = Transaction().connection.cursor()

        where = []

        # Recalculate only the cost of a given product
        # It had to be used only as a test because it doesn't recalculate the inputs in
        # case the product was a productino output
        if product_to_recompute:
            where.append("product = " + str(product_to_recompute))

        # Skip moves of non produced products and internal moves
        if skip_non_produced_products:
            where.append(
                "product in (" \
                "select distinct(product) " \
                "from stock_move " \
                "where production_output is not null and state='done')")

        # Reset product_cost_price__history (perhaps product_cost_price should...)
        if reset_product_cost_price__history:
            query = "delete from product_cost_price__history"
            if where:
                query += " where " + " and ".join(where)
            cursor.execute(query)

        # Internal moves don't affect cost but can contain incorrect cost_price
        # You can skipping assuming that...
        if skip_internal_moves:
            where.append(
                "(" \
                "    (l1.type in ('supplier', 'production') and l2.type ='storage') or " \
                "    (l1.type = 'storage' and l2.type = 'supplier') or" \
                "    (l1.type = 'storage' and l2.type = 'production')" \
                ")")

        where.append("state = 'done'")

        # Get moves ordered by effective_date instead of ordered by write_date as it was made before
        # because write_date could be updated by the programmed tasks.
        query = "select m.id " \
            "from stock_move m " \
            "join stock_location l1 on l1.id=m.from_location " \
            "join stock_location l2 on l2.id=m.to_location " \
            "where " + " and ".join(where) + \
            "order by effective_date asc, id asc"

        if limit:  # Expected number as limit to apply to query
            query = query + " limit " + str(limit)

        if offset:  # Expected number as offset to apply to query
            query = query + " offset " + str(offset)

        cursor.execute(query)
        moves = [cls(move[0]) for move in cursor.fetchall()]
        
        # Load stock. Stock dictionary: product_id, quantity, cost, write_date
        stock = {}
        if offset:
            offset_stock_file_name = log_path + "stock_" + str(offset) + ".csv"
            stock = load_offset_stock_from_csv(offset_stock_file_name)

        # Get industry location_id
        industry_locations = Location.search([('name', '=', 'Industry')])
        assert industry_locations, "Industry location not created yet! (Must be created as a supplier type location)."
        industry_location, = industry_locations 

        # Set context to calculate stock
        context = {}
        locations = Location.search([
            ('type', '=', 'storage'),
            ])
        context['with_childs'] = False
        context['locations'] = [l.id for l in locations]

        # Iterate in moves
        slice_counter = 0
        for moves_slice in grouped_slice(moves, slice_size):
            slice_counter = slice_counter + 1
            
            # Set logging
            from_move = (slice_counter - 1) * slice_size + offset
            to_move = from_move + slice_size
            logging = Logging(
                log_path + '/recompute_costs_moves_' + str(from_move) + '_to_' + str(to_move) + '_log.csv', [
                'move_type', 'move', 'product', 'quantity', 'stock_at_write_date',
                'unit_price', 'prev_unit_price', 'cost_price', 'prev_cost_price', 
                'write_date', 'origin'], from_move)

            for m in moves_slice:
                logging.add(0, 'move:' + str(m.id))
                m_dict = {}
                m_dict['prev_unit_price'] = m.unit_price
                m_dict['prev_cost_price'] = m.cost_price

                check_move(m)
                
                m_dict['move_type'] = (m.from_location.type if m.from_location != industry_location else 'industry') + \
                    '->' + m.to_location.type
                m_dict['move'] = m.id
                m_dict['product'] = m.product.id
                m_dict['quantity'] = m.quantity
                m_dict['stock_at_write_date'] = stock.get(m.product.id)['quantity'] if stock.get(m.product.id) else None
                m_dict['unit_price'] = m.unit_price
                m_dict['cost_price'] = m.cost_price
                m_dict['write_date'] = m.write_date
                m_dict['origin'] = m.origin
                logging.flush(m_dict)

            logging.close()

            # Save stock
            stock_file_name = log_path + "stock_" + str(logging.counter) + ".csv"
            save_stock_to_csv(stock, stock_file_name)

    @classmethod
    def production_set_cost_from_moves(cls):
        '''
        Probando... under construction!
        '''
        skip_non_produced_products = True
        limit = 1000
        offset = 0

        pool = Pool()
        # Move = pool.get('stock.move')
        Production = pool.get('production')
        productions = set()

        cursor = Transaction().connection.cursor()
        
        query = "select id from stock_move where state = 'done' " \
            "order by effective_date, id asc"
        if skip_non_produced_products:  # Skip moves of non produced products
            query = "select id " \
                "from stock_move " \
                "where state = 'done' " \
                "and product in (" \
                    "select distinct(product) " \
                    "from stock_move " \
                    "where production_output is not null and state='done'" \
                ") " \
                "and production_input is not null " \
                "order by effective_date asc, id asc"
        if limit:  # Expected number as limit to apply to query
            query = query + " limit " + str(limit)
        if offset:  # Expected number as offset to apply to query
            query = query + " offset " + str(offset)
        cursor.execute(query)
        moves = [cls(move[0]) for move in cursor.fetchall()]
        '''
        moves = Move.search([
                ('production_cost_price_updated', '=', True),
                ('production_input', '!=', None),
                ],
            order=[('effective_date', 'ASC')])
        '''
        for move in moves:
            if move.production_input not in productions:
                Production.set_cost([move.production_input])
                productions.add(move.production_input)
        cls.write(moves, {'production_cost_price_updated': False})