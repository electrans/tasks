# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta

__all__ = ['Location']


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    @classmethod
    def deactivate_movable_locations(cls):
        pool = Pool()
        Location = pool.get('stock.location')
        Product = pool.get('product.product')
        Move = pool.get('stock.move')
        Shipment = pool.get('stock.shipment.internal')
        Configuration = pool.get('stock.configuration')
        deactivate_locations = []
        create_shipments = []
        negative_moves = []

        locations = Location.search([('movable', '=', True)])
        for location in locations:
            if location.movable:
                pending_moves = Move.search(
                    [['OR', ('from_location', '=', location), ('to_location', '=', location)],
                     ('state', 'not in', ['done', 'cancel'])], limit=1)
                if not pending_moves:
                    leftovers_location = location.warehouse.leftovers_location.id \
                        if location.warehouse.leftovers_location else Configuration(1).leftovers_location.id
                    create_moves = []
                    negative_stock = False
                    for key, value in Product.products_by_location([location.id]).items():
                        if value > 0:
                            product = Product(key[1])
                            create_moves.append({'product': product, 'quantity': value, 'from_location': location,
                                                 'to_location': leftovers_location, 'uom': product.default_uom})
                        elif value < 0:
                            negative_stock = True
                            negative_moves.append(location)
                    # create internal shipment and moves, to empty the location if there is stock
                    if create_moves:
                        create_shipments.append({'from_location': location, 'to_location': leftovers_location,
                                                 'reference': 'Limpiar Pale', 'moves': [('create', create_moves)]})
                    # if the location don't have more stock and there are not pending moves, it gets deactivated
                    elif not negative_stock:
                        deactivate_locations.append(location)

        Shipment.create(create_shipments)
        Location.write(deactivate_locations, {'active': False})
