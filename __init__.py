# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import cron
from . import location
from . import purchase
from . import product
from . import stock
from . import expedient


def register():
    Pool.register(
        cron.Cron,
        location.Location,
        product.Template,
        purchase.Purchase,
        stock.Move,
        expedient.Expedient,
        module='electrans_tasks', type_='model')
