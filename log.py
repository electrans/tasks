from datetime import datetime
import csv


class Logging:
    __name__ = "electrans.logging"

    def __init__(self, file_name="./log.csv", extra_fields = None, offset = 0):
        headers = ["time", "level"]
        if extra_fields:
            headers = headers + extra_fields
        headers = headers + ["text"]
        self.f = open(file_name, 'wt')
        self.writer = csv.DictWriter(self.f, fieldnames=headers, extrasaction='ignore',
            quoting=csv.QUOTE_NONNUMERIC, lineterminator="\n",
            delimiter=',', quotechar='"')
        self.writer.writeheader()
        self.counter = offset
        self.level = 0
        self.text = ""
        self.start_time = datetime.now()
        print('\n')
        print(('Logging to ' + file_name))
        print('---------------------------------------------------------')

    def log(self, level, text, extra_data=None):
        #TODO: Change by logging (see delete_duplicated_lots.py i.e.)

        level_text = "?"
        if level == 0:
            level_text = "INFO"
        elif level == 1:
            level_text = "WARNING"
        elif level == 2:
            level_text = "ERROR"

        self.counter = self.counter + 1
        #print((datetime.now().strftime("%H:%M:%S") + " " + str(datetime.now()-self.start_time)[:7] + \
        #    " #" + str(self.counter) + " " + level_text + " " + text))
        #if level > 0:  # Trying to get faster
        print(level_text + " #" + str(self.counter) + " " + str(datetime.now()-self.start_time)[:7] + " " + text)

        d = {}
        d["time"] = datetime.now().time()
        d["level"] = level_text
        if extra_data:
            d.update(extra_data)
        d["text"] = text
        self.writer.writerow(d)

    def add(self, level, text):
        self.level = level if level > self.level else self.level
        self.text = text if self.text == "" else self.text + text
    
    def flush(self, extra_data=None):
        self.log(self.level, self.text, extra_data)
        self.level = 0
        self.text = ""

    def close(self):
        self.f.close()