# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta

__all__ = ['PurchaseRequest', 'ApprovalRequest']


class PurchaseRequest(metaclass=PoolMeta):
    __name__ = 'purchase.request'

    @classmethod
    def update_pending_requests_state(cls):
        Request = Pool().get('purchase.request')
        Request.update_state(Request.search([('state', '=', 'pending')]))


class ApprovalRequest(metaclass=PoolMeta):
    __name__ = 'approval.request'

    @classmethod
    def update_purchase_approval(cls):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        Request = pool.get('approval.request')
        requests = []
        for request in Request.search([]):
            if request.document and isinstance(request.document, Purchase):
                request.untaxed_amount = request.document.untaxed_amount
                request.party = request.document.party
                requests.append(request)
        Request.save(requests)
