# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class Expedient(metaclass=PoolMeta):
    __name__ = 'expedient.expedient'

    @classmethod
    def set_expedients_data(cls):
        pool = Pool()
        NewExpedient = pool.get('expedient.expedient')
        Attachment = pool.get('ir.attachment')
        query = """select id, private, subject, description from expedient order by id"""
        cursor = Transaction().connection.cursor()
        cursor.execute(query)
        expedients = cursor.fetchall()
        for expedient in expedients:
            new_id, = NewExpedient.create([{'private': expedient[1],
                                            'subject': expedient[2],
                                            'description': expedient[3]
                                            }])
            if new_id.id == 105:
                NewExpedient.create([{}])
            attachments = Attachment.search([('resource', '=', 'expedient,' + str(expedient[0]))])
            if attachments:
                for attachment in attachments:
                    cursor.execute("UPDATE ir_attachment SET resource =  'expedient.expedient," +
                                   str(new_id.id) + "' WHERE id = " + str(attachment.id))