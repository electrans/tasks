# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta

__all__ = ['Purchase']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def update_shipment_state(cls):
        Purchase = Pool().get('purchase.purchase')
        purchases = Purchase.search([])
        cancel_purchases = []
        for p in purchases:
            if p.moves and all(m.state == 'cancel' for m in p.moves):
                cancel_purchases.append(p)
        Purchase.write(cancel_purchases, {'shipment_state': 'canceled'})

    @classmethod
    def update_invoice_state(cls):
        Purchase = Pool().get('purchase.purchase')
        purchases = Purchase.search([('invoice_state', '=', 'waiting')])
        to_write = []
        for p in purchases:
            if p.get_invoice_state() == 'posted':
                to_write.append(p)
        Purchase.write(to_write, {'invoice_state': 'posted'})
