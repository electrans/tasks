# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
try:
    from trytond.modules.electrans_tasks.tests.test_electrans_tasks import suite
except ImportError:
    from .test_electrans_tasks import suite

__all__ = ['suite']
